# Código Colibrí #

Este repositorio contiene el código correspondiente al proyecto Colibrí, desarrollado por el equipo de desarrollo Paire para la unidad académica de Río Gallegos de la universidad nacional de la Patagonia Austral.

### Estructura de este respositorio ###

El código almacenado en este repositorio se puede separar en dos categorías distintas:

* Aplicación web
* Aplicación móvil

El desarrollo de la aplicación móvil se lleva a cabo utilizando Ionic framework, por lo tanto utiliza las tecnologías que este abarca (AngularJS, TypeScript, Sass, entre otras).
Por otro lado, la construcción de la aplicación web se basa sobre el módulo "UARG flow", desarrollado por Eder dos Santos, razón por la cual se siguen varios estilos de codificación y diseño del mismo.