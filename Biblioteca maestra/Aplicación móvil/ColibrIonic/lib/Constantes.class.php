<?php

include_once '../gui/GUI.class.php';
setlocale(LC_TIME, 'es_AR.utf8');

/**
 * Esta clase almacena las constantes utilizadas dentro del sistema.
 * @author Ariel Machini <arielmachini@protonmail.com>
 */
class Constantes {

    const NOMBRE_SISTEMA = "Sistema Colibrí";
    
    const WEBROOT = "/var/www/html/ColibrIonic/";
    const APPDIR = "ColibrIonic";
        
    const SERVER = "http://esi.uarg.unpa.edu.ar";
    const APPURL = "http://esi.uarg.unpa.edu.ar/ColibrIonic";
    const HOMEURL = "http://esi.uarg.unpa.edu.ar/ColibrIonic/recuperarFormularios.php";
    const HOMEAUTH = "http://esi.uarg.unpa.edu.ar/ColibrIonic/recuperarFormularios.php";
    
    const BD_SCHEMA = "uargflow";
    const BD_USERS = "uargflow";
    
}
