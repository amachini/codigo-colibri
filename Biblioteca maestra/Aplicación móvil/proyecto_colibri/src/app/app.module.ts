import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { AboutPage } from '../pages/about/about';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ConectorProvider } from '../providers/conector/conector';

/* Importaciones propias */
import { FormulariosPage } from '../pages/formularios/formularios';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { PerfilPage } from '../pages/perfil/perfil';
import { VerFormularioPage } from '../pages/ver-formulario/ver-formulario';

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    HomePage,
    TabsPage,
    FormulariosPage,
    PerfilPage,
    VerFormularioPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule, // Agregado manualmente
    HttpModule // Agregado manualmente
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    HomePage,
    TabsPage,
    FormulariosPage,
    PerfilPage,
    VerFormularioPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ConectorProvider
  ]
})
export class AppModule {}
