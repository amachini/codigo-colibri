import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

/* Importaciones propias */
import 'rxjs/add/operator/map';
import { Response } from '@angular/http'

@Injectable()
export class ConectorProvider {

  // host: string = "http://localhost/"
  host: string = "http://170.210.93.100:8103/";

  constructor(public http: HttpClient) {
    console.log('Conexión a la base de datos establecida.');
  }

  private recuperarInformacion(formulario: Response) {
    let cuerpo = formulario;
    return cuerpo || { };
  }

  /*enviarSolicitud($datos) {
    let cabecera = new HttpHeaders();
    cabecera.append("Content-Type", "application/json");
    this.http.post(this.host + "ColibrIonic/procesarSolicitud.php", $datos, cabecera);
  }*/

  recuperarFormularios() {
    return this.http.get(this.host + "ColibrIonic/recuperarFormularios.php").map(this.recuperarInformacion);
  }

  recuperarCampos(id: number) {
    return this.http.get(this.host + "ColibrIonic/recuperarCampos.php?id=" + id).map(this.recuperarInformacion);
  }

  recuperarFormulario(id: number) {
    return this.http.get(this.host + "ColibrIonic/recuperarFormulario.php?id=" + id).map(this.recuperarInformacion);
  }

}
