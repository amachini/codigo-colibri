import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

/* Importaciones propias */
import { FormulariosPage } from '../formularios/formularios';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {

  }

  verFormularios() {
    this.navCtrl.parent.select(1);
  }

}
