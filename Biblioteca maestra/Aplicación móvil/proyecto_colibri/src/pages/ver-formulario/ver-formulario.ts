import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/* Importaciones propias */
import { AlertController } from 'ionic-angular';
import { ConectorProvider } from '../../providers/conector/conector';

@IonicPage()
@Component({
  selector: 'page-ver-formulario',
  templateUrl: 'ver-formulario.html',
})
export class VerFormularioPage {

  formulario: any;
  camposFormulario: any[99];

  // datos: any = {};

  constructor(public navCtrl: NavController, public navParams: NavParams, private alertCtrl: AlertController, private servicioConector: ConectorProvider) {
    let idFormulario = navParams.get('idFormulario');
    this.formulario = ""; // Se inicializa sólo porque Angular se queja.

    this.recuperarFormulario(idFormulario);
    this.recuperarCampos(idFormulario);
  }

  ionViewDidLoad() {}

  /*enviarSolicitud() {
    let datos = '[';

    this.camposFormulario.forEach(campo => {
      datos += '{"valor": ' + this.datos.campo.titulo + '}, '; // NO es posible... encontrar cómo darle la vuelta de otra forma.
    });

    datos = datos.substring(0, datos.length - 2);
    datos += ']';

    JSON.stringify(datos);

    this.servicioConector.enviarSolicitud(datos);
  }*/

  recuperarCampos(id: number) {
    this.servicioConector.recuperarCampos(id).subscribe((camposFormulario: Response) => this.camposFormulario = camposFormulario, error => console.log("E: Se produjo el siguiente error al intentar recuperar los campos del formulario de la base de datos: " + error));
  }

  recuperarFormulario(id: number) {
    this.servicioConector.recuperarFormulario(id).subscribe((formulario: Response) => this.formulario = formulario, error => console.log("E: Se produjo el siguiente error al intentar recuperar el formulario de la base de datos: " + error));
  }

  alertarWIP() {
    let mensaje = this.alertCtrl.create({
      title: 'WIP',
      subTitle: 'Esta funcionalidad todavía no está desarrollada.',
      buttons: ['Cerrar']
    });
    mensaje.present();
  }

}
