import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/* Importaciones propias */
import { AlertController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-perfil',
  templateUrl: 'perfil.html',
})
export class PerfilPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, private alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
    let mensaje = this.alertCtrl.create({
      title: 'WIP',
      subTitle: 'Esta página todavía no fue desarrollada.',
      buttons: ['Volver']
    });
    mensaje.present();
  }

}
