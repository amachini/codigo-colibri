import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/* Importaciones propias */
import 'rxjs/add/operator/debounceTime';
import { ConectorProvider } from '../../providers/conector/conector';
import { FormControl } from '@angular/forms';
import { VerFormularioPage } from '../ver-formulario/ver-formulario';

@IonicPage()
@Component({
  selector: 'page-formularios',
  templateUrl: 'formularios.html',
})
export class FormulariosPage {

  formularios: any[999];
  private formulariosAuxiliar: any[999];

  private buscandoResultados: any = false;
  private nombreABuscar: string = "";
  private controladorBusqueda: FormControl;

  constructor(public navCtrl: NavController, public navParams: NavParams, private servicioConector: ConectorProvider) {
    this.controladorBusqueda = new FormControl();
    this.formulariosAuxiliar = [];
    this.recuperarFormularios();
  }

  ionViewDidLoad() {
    this.controladorBusqueda.valueChanges.debounceTime(300).subscribe(nombreABuscar => {
      this.buscandoResultados = false;
      this.listarResultados();
    });
  }

  recuperarFormularios() {
    this.servicioConector.recuperarFormularios().subscribe((formularios: Response) => this.formularios = formularios, error => console.log("E: Se produjo el siguiente error al intentar recuperar los formularios de la base de datos: " + error));
  }

  verFormulario(id: number) {
    this.navCtrl.push(VerFormularioPage, {
      idFormulario: id
    });
  }

  /* Búsqueda de formularios */
  listarResultados() {
    if (this.nombreABuscar.trim() === "") {
      this.recuperarFormularios();
    } else {
      this.formularios = this.filtrar();
    }
  }

  private filtrar() {
    this.servicioConector.recuperarFormularios().subscribe((formularios: Response) => this.formulariosAuxiliar = formularios, error => console.log("E: Se produjo el siguiente error al intentar recuperar los formularios de la base de datos: " + error));

    return this.formulariosAuxiliar.filter((formulario) => {
      return formulario.titulo.toLowerCase().indexOf(this.nombreABuscar.toLowerCase()) > -1;
    });
  }

  realizarBusqueda() {
    this.buscandoResultados = true;
  }

}
